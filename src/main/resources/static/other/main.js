document.addEventListener('DOMContentLoaded', () => {
  const formulario = document.getElementById('formulario');
  const comunaSelect = document.getElementById('comuna');
  const campoList = document.getElementById('campo-list');

  // Cargar comunas
  fetch('http://localhost:8080/api/comunas')
    .then(response => response.json())
    .then(data => {
      data.forEach(comuna => {
        const option = document.createElement('option');
        option.value = comuna.comuna.code;
        option.textContent = comuna.comuna.name;
        comunaSelect.appendChild(option);
      });
    });

  // Manejar envío del formulario
  formulario.addEventListener('submit', async (event) => {
        event.preventDefault();

        const nombre = document.getElementById('nombre').value;
        const apellido = document.getElementById('apellido').value;
        const telefono = document.getElementById('telefono').value;
        const comuna = comunaSelect.options[comunaSelect.selectedIndex].text;

        const datos = {
          nombre: nombre,
          apellido: apellido,
          telefono: telefono,
          comuna: comuna
        };

        try {
          const response = await fetch('http://localhost:8080/api/guardar', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(datos)
          });

          if (response.ok) {
            console.log('Datos enviados con éxito');
            formulario.reset();
            await cargarCampos();
          } else {
            console.error('Error al enviar los datos');
          }
        } catch (error) {
          console.error('Error en la solicitud:', error);
        }
      });

       async function cargarCampos() {
          campoList.innerHTML = ''; // Limpiar la lista

          // Lógica para cargar datos desde el back-end
          const response = await fetch('http://localhost:8080/api/listar');
          const data = await response.json();

          data.forEach(campo => {
            const listItem = document.createElement('li');
            listItem.textContent = `${campo.id} - ${campo.nombre} ${campo.apellido} - ${campo.telefono} - ${campo.comuna}`;
            campoList.appendChild(listItem);
          });
        }

        // Cargar los campos al cargar la página
        cargarCampos();
    });
