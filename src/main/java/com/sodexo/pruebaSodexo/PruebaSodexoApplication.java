package com.sodexo.pruebaSodexo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class PruebaSodexoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaSodexoApplication.class, args);
	}

}
