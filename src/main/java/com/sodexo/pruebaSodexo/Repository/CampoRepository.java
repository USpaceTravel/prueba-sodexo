package com.sodexo.pruebaSodexo.Repository;

import com.sodexo.pruebaSodexo.Model.Campo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CampoRepository extends JpaRepository<Campo, Long> {

}
