package com.sodexo.pruebaSodexo.Controller;

import com.sodexo.pruebaSodexo.Model.Campo;
import com.sodexo.pruebaSodexo.Model.Comuna;
import com.sodexo.pruebaSodexo.Service.CampoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class CampoController {
    @Autowired
    private CampoService campoService;

    @GetMapping("/comunas")
    public List<Comuna> getComunas() {
        return campoService.getComunas();
    }

    @PostMapping("/guardar")
    public ResponseEntity<Campo> guardarCampo(@RequestBody Campo campo) {
        Campo nuevoCampo = campoService.guardarCampo(campo);
        return new ResponseEntity<>(nuevoCampo, HttpStatus.CREATED);
    }

    @GetMapping("/listar")
    public List<Campo> listarCampos() {

        return campoService.listarCampos();
    }

}
