package com.sodexo.pruebaSodexo.Model;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.processing.Generated;
import java.util.LinkedHashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "comuna"
})
@Generated("jsonschema2pojo")
public class Comuna {

    @JsonProperty("comuna")
    private Object comuna;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new LinkedHashMap<>();

    @JsonProperty("comuna")
    public Object getComuna() {
        return comuna;
    }

    @JsonProperty("comuna")
    public void setComuna(Object comuna) {
        this.comuna = comuna;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}


