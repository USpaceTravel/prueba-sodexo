package com.sodexo.pruebaSodexo.Service;

import com.sodexo.pruebaSodexo.Model.Campo;
import com.sodexo.pruebaSodexo.Model.Comuna;
import com.sodexo.pruebaSodexo.Repository.CampoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CampoService {
    @Autowired
    private CampoRepository campoRepository;

    @Autowired
    private ComunaFeignClient comunaFeignClient;

    public List<Comuna> getComunas() {
        return comunaFeignClient.getComunas();
    }

    public Campo guardarCampo(Campo campo) {
        return campoRepository.save(campo);
    }

    public List<Campo> listarCampos() {
        return campoRepository.findAll();
    }


}
