package com.sodexo.pruebaSodexo.Service;

import com.sodexo.pruebaSodexo.Model.Comuna;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "comunaClient", url = "https://private-anon-53deb82188-gonzalobulnes.apiary-mock.com")
public interface ComunaFeignClient {

    @GetMapping("/comunas")
    List<Comuna> getComunas();
}

